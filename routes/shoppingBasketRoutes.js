// Require your dependencies

const express = require("express");

const router = express.Router();

const model = require('../model/products');

// Turn the payload of a POST request from text to an object of key/value pairs

router.use(express.urlencoded({extended: true}));

// This route is used for adding/removing items to/from the basket
// on the server side.
router.post("/basket-items/:action", function(request, response) {
    const params = request.params;
    const action = params.action;

    const id = request.body.id;

    const item = model.findById(id);

    // If there is no item.
    if (!item) {
        return response.sendStatus(404);
    }

    if (action === "add") {
        request.shoppingBasket.add(id);
    }

    if (action === "remove") {
        if (request.shoppingBasket.doesNotHave(id)) {
            return response.sendStatus(404);
        }

        request.shoppingBasket.remove(id);
    }

    response.sendStatus(200);
});

module.exports = router;
