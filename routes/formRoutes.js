const express = require("express");

const noodlemania = require("../model/noodlemania");

const router = express.Router();

// Turn the payload of a POST request from text to an object of key/value pairs

router.use(express.urlencoded({extended: true}));

router.post("/contact-us", function(request, response) {
    const values = {
        "First name": request.body["first-name"],
        "Last name": request.body["last-name"],
        "Phone number": request.body["phone"],
        "E-mail": request.body["email"],
        "Message": request.body["message"]
    };

    response.render("layout", {
        pageTitle: "Message sent!",
        content: "message-sent",
        logo: noodlemania.logo,
        basketSize: request.shoppingBasket.getSize(),
        sender: values["First name"],
        message: values.Message
    });
});

router.post("/checkout", function(request, response) {
    const name = request.body["full-name"];

    response.render("payment-results", {
        pageTitle: "Payment complete!",
        content: "payment-results",
        logo: noodlemania.logo,
        name: name
    });
});

module.exports = router;
