const express = require("express");

const noodlemania = require("../model/noodlemania");

const model = require('../model/products');

const router = express.Router();

router.get("/", function(request, response) {
    response.render("layout", {
        pageTitle: "Noodlemania | Home",
        content: "index",
        logo: noodlemania.logo,
        basketSize: request.shoppingBasket.getSize()
    });
});

router.get("/about-us", function(request, response) {
    response.render("layout", {
        pageTitle: "Noodlemania | About Us",
        content: "about-us",
        logo: noodlemania.logo,
        basketSize: request.shoppingBasket.getSize()
    });
});

router.get("/contact-us", function(request, response) {
    response.render("layout", {
        pageTitle: "Noodlemania | Contact Us",
        content: "contact-us",
        logo: noodlemania.logo,
        noodlemaniaPhoneNumber: noodlemania.phoneNumber,
        noodlemaniaAddress: noodlemania.address,
        basketSize: request.shoppingBasket.getSize()
    });
});

router.get("/basket", function(request, response) {
    const itemsAddedToTheBasket = request.shoppingBasket.getDisplayableItems();

    response.render("layout", {
        pageTitle: "Noodlemania | Your Basket",
        content: "basket",
        logo: noodlemania.logo,
        basketSize: request.shoppingBasket.getSize(),
        items: itemsAddedToTheBasket,
        totalPrice: request.shoppingBasket.getTotalPrice(),
        penceToPounds: model.penceToPounds
    });
});

router.get("/checkout", function(request, response) {
    const itemsAddedToTheBasket = request.shoppingBasket.getDisplayableItems();

    response.render("layout", {
        pageTitle: "Noodlemania | Checkout",
        content: "checkout",
        logo: noodlemania.logo,
        basketSize: request.shoppingBasket.getSize(),
        items: itemsAddedToTheBasket,
        totalPrice: request.shoppingBasket.getTotalPrice(),
        penceToPounds: model.penceToPounds
    });
});

module.exports = router;
