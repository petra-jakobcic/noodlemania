// Require the dependencies

const express = require("express");

const router = express.Router();

const model = require('../model/products');

const noodlemania = require("../model/noodlemania.json");

// Define the routes

router.get("/food", function(request, response) {
    response.render("layout", {
        pageTitle: "Noodlemania | Menu",
        content: "food",
        logo: noodlemania.logo,
        items: model.findByType("food"),
        basketSize: request.shoppingBasket.getSize(),
        penceToPounds: model.penceToPounds
    });
});

router.get("/food-info/:slug", function(request, response) {
    // Route parameters are pieces of a URL.
    // Express can capture these pieces as key-value pairs.
    // The captured values are populated in the 'request.params' object.
    // The bit with the colon in the path becomes the respective key (slug).
    const params = request.params;
    const slug = params.slug;

    const item = model.findBySlug(slug);

    if (item === undefined) {
        // Send an error page in case there is no matching slug.
        const path = require("path");
        response.sendFile(path.resolve("views/404.html"));
    } else {
        // Render a food item page in case there is a matching slug.
        response.render("layout", {
            pageTitle: "Noodlemania | Food info",
            content: "food-info",
            logo: noodlemania.logo,
            foodItem: item,
            basketSize: request.shoppingBasket.getSize()
        });
    }
});

router.get("/drinks", function(request, response) {
    const drinks = model.findByType("drink");
    const categories = ["soft drinks", "smoothies", "beer", "coffee"];
    const byCategory = {};

    categories.forEach(function(category) {
        const list = drinks.filter(function(drink) {
            return drink.category === category;
        });

        byCategory[category] = list;
    });

    response.render("layout", {
        pageTitle: "Noodlemania | Drinks",
        content: "drinks",
        logo: noodlemania.logo,
        basketSize: request.shoppingBasket.getSize(),
        penceToPounds: model.penceToPounds,
        softDrinks: byCategory["soft drinks"],
        smoothies: byCategory["smoothies"],
        beer: byCategory["beer"],
        coffee: byCategory["coffee"]
    });
});

module.exports = router;
