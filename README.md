# 🍜 Noodlemania

Noodlemania! File your tax returns, enjoy noodles and then un-jam the printer!

## Installation

1. Clone the project ([Bitbucket repository is here](https://bitbucket.org/petra-jakobcic/noodlemania/src/master/)).   
   ```
   git clone https://bitbucket.org/petra-jakobcic/noodlemania.git
   ```
1. Install the NPM dependencies:   
   ```
   npm install
   ```
1. Start the server.   
   ```
   npm start
   ```
