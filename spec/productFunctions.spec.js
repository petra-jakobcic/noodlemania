const createProductModel = require('../model/createProductModel');

const testData = [
    {
        "id": "111",
        "type": "food",
        "slug": "test-product1",
        "image": "/images/img1.jpg",
        "alt": "Test product alt 1",
        "name": "Test product name 1",
        "price": 123,
        "description": "Test description 1.",
        "allergens": ["a1", "a2", "a3"]
    },
    {
        "id": "222",
        "type": "drink",
        "slug": "test-product2",
        "image": "/images/img2.jpg",
        "alt": "Test product alt 2",
        "name": "Test product name 2",
        "price": 456,
        "description": "Test description 2.",
        "allergens": ["a4", "a5", "a6"]
    }
];

describe('The product model', () => {
    it('gets all the products', () => {
        const model = createProductModel(testData);
        const data = model.getAll();

        expect(data).toBe(testData);
    });

    it('finds a product with the matching ID', () => {
        const model = createProductModel(testData);
        const data = model.findById("111");

        expect(data).toBe(testData[0]);
    });

    it('throws an error message when searching by a non-existent ID', () => {
        const model = createProductModel(testData);

        expect(() => {
            model.findById("333");
        }).toThrow("This ID does not exist.");
    });

    it('finds a food item with the matching slug', () => {
        const model = createProductModel(testData);
        const data = model.findBySlug("test-product1");

        expect(data).toBe(testData[0]);
    });

    it('throws an error message when searching by a non-existent slug', () => {
        const model = createProductModel(testData);

        expect(() => {
            model.findBySlug('test-product3');
        }).toThrow("This slug does not exist.");
    });

    it('finds a list of only 1 product when searching by type', () => {
        const model = createProductModel(testData);
        const matchingProducts = model.findByType("drink");

        expect(matchingProducts.length).toBe(1);
    });

    it('gives an empty list when searching by a non-existent type', () => {
        const model = createProductModel(testData);
        const matchingProducts = model.findByType("alcohol");

        expect(matchingProducts.length).toBe(0);
    });

    it('formats a typical number of pence expressed in pounds', () => {
        const model = createProductModel(testData);
        const pounds = model.penceToPounds(4526);

        expect(pounds).toBe("45.26");
    });

    describe('Penny-to-pounds method', () => {
        it('formats a penny to pounds', () => {
            const model = createProductModel(testData);
            const pounds = model.penceToPounds(1);

            expect(pounds).toBe("0.01");
        });

        it('formats a large number of pence to pounds', () => {
            const model = createProductModel(testData);
            const pounds = model.penceToPounds(123456);

            expect(pounds).toBe("1234.56");
        });

        it('throws a useful error on a negative number', () => {
            const model = createProductModel(testData);

            expect(() => {
                model.penceToPounds(-5);
            }).toThrow("Only positive numbers allowed.");
        });

        it('throws a useful error on pence with a decimal point', () => {
            const model = createProductModel(testData);

            expect(() => {
                model.penceToPounds(0.01);
            }).toThrow("Only whole numbers allowed.");
        });

        it('throws a useful error on a large amount of pence with a decimal point', () => {
            const model = createProductModel(testData);

            expect(() => {
                model.penceToPounds(123456.78);
            }).toThrow("Only whole numbers allowed.");
        });
    });
});
