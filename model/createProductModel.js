// Write the functions we need

/**
 * @param {Array} products A list of product data.
 * 
 * @returns {Object} The model.
 */
function createProductModel(products) {
    /**
     * Gets all the products.
     * 
     * @returns {Array} A list of all the products.
     */
    function getAll() {
        return products;
    }

    /**
     * Finds a product with the matching ID.
     *
     * @param {string} id The ID.
     * 
     * @returns {Object} A product object.
     */
    function findById(id) {
        // I need to get all the products.
        // Then, I need to compare the IDs.
        // Finally, I need to return the product with the matching ID.

        const products = getAll();

        const result = products.find(function(product) {
            return product.id === id;
        });

        if (result === undefined) {
            throw "This ID does not exist.";
        }

        return result;
    }

    /**
     * Finds a food item with the matching slug.
     * 
     * @param {string} slug The slug.
     * 
     * @returns {Object} A food item object.
     */
    function findBySlug(slug) {
        // I need to get all the food items.
        // Then, I need to compare the slugs.
        // Finally, I need to return the food item with the matching slug.

        const allFood = findByType("food");

        const result = allFood.find(function(currentFoodItem) {
            return currentFoodItem.slug === slug;
        });

        if (result === undefined) {
            throw "This slug does not exist.";
        }

        return result;
    }

    /**
     * Finds all products of a certain type.
     * 
     * @param {string} type The type of a product.
     * 
     * @returns {Array} A list of products with the given type.
     */
    function findByType(type) {
        const products = getAll();

        return products.filter(function(product) {
            return product.type === type;
        });
    }

    /**
     * Converts a number to a string.
     *
     * @param {number} number The number.
     *
     * @returns {string} The number as a string.
     */
    function numberToString(number) {
        return number.toString();
    }

    /**
     * Adds zeros to the left of any string
     * that is less than 3 characters long,
     * otherwise returns the original string.
     *
     * @param {string} string The string to pad.
     *
     * @returns {string} The padded string, if necessary,
     *                   otherwise the original string.
     */
    function padStringLeft(string) {
        return string.padStart(3, "0");
    }

    /**
     * Adds a dot to the formatted number of pence expressed in pounds.
     *
     * @param {number} pence The amount of pennies.
     *
     * @returns {string} The formatted number of pence as pounds.
     * 
     * @throws {string} If the input is negative. 
     */
    function penceToPounds(pence) {
        if (pence < 0) {
            throw "Only positive numbers allowed.";
        }

        if (!Number.isInteger(pence)) {
            throw "Only whole numbers allowed.";
        }

        // Firstly, turn the number into a string.
        const penceAsString = numberToString(pence);

        // Secondly, pad the string with zeros, if needed.
        const paddedPence = padStringLeft(penceAsString);

        // Thirdly, add the dot two spaces before the end of the string.
        return paddedPence.substr(0, paddedPence.length - 2) +
            "." +
            paddedPence.substr(-2);
    }

    return {
        getAll,
        findById,
        findByType,
        findBySlug,
        penceToPounds
    };
}

module.exports = createProductModel;
