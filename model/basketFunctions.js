// Require your dependencies

const model = require('./products');

/*
 * Create an array which we will
 * add product IDs to.
 * The array may contain the same ID many times
 * if the user has added the same product
 * more than once. We only need to add IDs
 * and not the full set of product details
 * (e.g. name, slug, description etc.).
 */
// const basket = [];

/**
 * Creates a new shopping basket.
 *
 * @param {Array} basket An array of product IDs
 *                       (typically taken from a session).
 *
 * @returns {Object} A shopping basket object that directly changes ( ! )
 *                   the given array as it gets used.
 */
function createBasket(basket) {
    /**
     * Adds a product to the basket based on the product's ID.
     *
     * @param {string} id The product's ID.
     */
    function add(id) {
        basket.push(id);
    }

    /**
     * Removes a product from the basket based on the product's ID.
     *
     * @param {string} id The product's ID.
     */
    function remove(id) {
        for (let i = 0; i < basket.length; i++) {
            let currentId = basket[i];

            if (currentId === id) {
                basket.splice(i, 1);
                return;
            }
        }
    }

    /**
     * Gets the size of the basket.
     *
     * @returns {number} The size of the basket.
     */
    function getSize() {
        return basket.length;
    }

    /**
     * Checks if the basket contains the given ID.
     *
     * @param {string} id The product ID.
     *
     * @returns {boolean} True if the basket contains the ID,
     *                    otherwise false.
     */
    function doesHave(id) {
        // Returns true if the basket contains the ID.
        return basket.indexOf(id) !== -1;
    }

    /**
     * Checks if the basket contains the given ID.
     *
     * @param {string} id The product ID.
     *
     * @returns {boolean} True if the basket does NOT contain
     *                    the ID, otherwise false.
     *
     */
    function doesNotHave(id) {
        return !doesHave(id);
    }

    /**
     * Prepares the shopping basket items in a way that
     * is ready to be displayed on the view page.
     *
     * @returns {Object} An object containing the customer's items and
     *                   their quantities ready to be checked out.
     */
    function getItems() {
        // The basket is available to us.
        // We can also use the products model.
        // Turn the array of IDs into an array of objects.

        const result = basket.reduce(function(accumulator, id) {
            // If the 'id' exists as a key in the accumulator
            if (id in accumulator) {
                // Increment the quantity
                accumulator[id].quantity += 1;
            } else {
                let product = model.findById(id);
                // Make a copy of one product
                let copyOfProduct = { ...product };
                copyOfProduct.quantity = 1;

                accumulator[id] = copyOfProduct;
            }

            return accumulator;
        }, {});

        return result;
    }

    /**
     * Gets the total price of items in the basket.
     *
     * @returns {number} The total price of items in the basket.
     */
    function getTotalPrice() {
        let totalPrice = 0;

        basket.forEach(function(id) {
            const product = model.findById(id);
            totalPrice += parseFloat(product.price);
        });

        return totalPrice;
    }

    return {
        add: add,
        remove: remove,
        getSize: getSize,
        getDisplayableItems: getItems,
        doesHave: doesHave,
        doesNotHave: doesNotHave,
        getTotalPrice: getTotalPrice
    };
}

module.exports = createBasket;
// module.exports = {
//     add: add,
//     remove: remove,
//     getSize: getSize,
//     getDisplayableItems: getItems,
//     doesHave: doesHave,
//     doesNotHave: doesNotHave,
//     getTotalPrice: getTotalPrice
// };
