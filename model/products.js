const createProductModel = require("./createProductModel");
const data = require("./products.json");

const model = createProductModel(data);

module.exports = model;
