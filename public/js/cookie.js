// Grab the element we need.

const greeting = document.getElementById("greeting-heading");

/**
 * Gets the number of seconds until the expiration time.
 *
 * @param {number} days The number of days until the expiration time.
 *
 * @returns {number} The seconds until the expiration time.
 */
function setExpirationTime(days) {
    return days * (60 * 60 * 24);
}

/**
 * Sets a cookie to store the user's name when the user visits the
 * website for the first time.
 *
 * @param {string} cname Cookie name.
 * @param {string} cvalue Cookie value.
 * @param {number} expiration The number of days until the cookie expires.
 */
function setCookie(cname, cvalue, exdays) {
    const currentDateAndTime = new Date();

    const expirationTime = currentDateAndTime.setTime(currentDateAndTime.getTime() + setExpirationTime(exdays));

    let expires = "expires=" + expirationTime.toUTCString();

    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * Gets the cookie.
 *
 * @param {string} cname The cookie name.
 */
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

/**
 * Checks if the cookie exists.
 */
function checkCookie() {
    let username = getCookie("username");

    if (username) {
        greeting.innerHTML = `Welcome to Noodlemania, ${username}!`;
    } else {
        username = prompt("Please enter your name:", "");

        if (username !== "" && username !== null) {
            setCookie("username", username, 365);
        }
    }
}

checkCookie();
