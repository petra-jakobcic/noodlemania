// Grab the elements we need
const form = document.getElementById("form-contact");

const firstNameField = document.getElementById("first-name");
const lastNameField = document.getElementById("last-name");
const phoneField = document.getElementById("phone");
const emailField = document.getElementById("email");
const messageField = document.getElementById("message");

const errorsFirstNameDiv = document.getElementById("errors-firstname");
const errorsLastNameDiv = document.getElementById("errors-lastname");
const errorsEmailDiv = document.getElementById("errors-email");
const errorsPhoneDiv = document.getElementById("errors-phone");

// Create an object with error messages

const errorMessages = {
    tooShort: "The name is too short.",
    tooLong: "The name is too long.",
    mustContainLetters: "The name must contain letters only.",
    invalidEmail: "This is not a valid e-mail address.",
    invalidPhone: "This is not a valid phone number."
}

// Write the functions we need

/**
 * Validates the user's name.
 * 
 * @param {string} name The user's name.
 * 
 * @returns {Array<string>} An empty array if everything's ok, otherwise an array with error messages.
 */
function validateName(name) {
    const messages = [];
    const pattern = /^[a-zA-Z ]+$/;

    if (name.length < 2) {
        messages.push(errorMessages.tooShort);
    }

    if (name.length > 20) {
        messages.push(errorMessages.tooLong);
    }

    if (!name.match(pattern)) {
        messages.push(errorMessages.mustContainLetters);
    }

    return messages;
}

/**
 * Validates the user's phone number.
 * 
 * @param {number} number The user's phone number.
 * 
 * @returns {Array} An empty array if everything's ok, otherwise an array with error messages.
 */
function validatePhoneNumber(phoneNumber) {
    // A plus character (the question mark means optional)
    // followed by one or more digits ("\d" means "digit" i.e. 0-9).
    const pattern = /^\+?\d+$/;

    const messages = [];

    if (!phoneNumber.match(pattern) || phoneNumber.length < 11) {
        messages.push(errorMessages.invalidPhone);
    }

    return messages;
}

/**
 * Validates the user's e-mail address.
 * 
 * @param {string} email The user's e-mail address.
 * 
 * @returns {Array} An empty array if everything's ok, otherwise an array with error messages.
 */
function validateEmail(email) {
    // The square brackets represent ONE character.
    // Inside the square brackets, you can specify all characters
    // that are allowed.
    // By using the caret symbol (^) in the square brackets, you can
    // specify the characters that are NOT allowed.
    // Here we can see [^@]+ which means:
    // Any character which is NOT an @ symbol.
    // The plus sign after it means "one or more".
    // To summarise, that means "one or more characters that are not @".

    // Read this pattern as:
    // "One or more non @ characters"
    // "Followed by ONE @ character"
    // "Followed by one or more non @ characters"
    // "Followed by a dot (\.)"
    // "Followed by one or more non @ characters"
    const pattern = /^[^@]+@[^@]+\.[^@\.]+$/;

    const messages = [];

    if (!email.match(pattern)) {
        messages.push(errorMessages.invalidEmail);
    }

    return messages;
}

/**
 * Creates an li containing a message.
 *
 * @param {string} string The message.
 *
 * @returns {HTMLLIElement} An li element.
 */
function messageToLi(message) {
    const li = document.createElement("li");
    li.innerHTML = message;
    li.classList.add("error-message");

    return li;
}

/**
 * Creates an unordered list of error messages.
 *
 * @param {Array} array An array of error messages.
 *
 * @returns {HTMLUListElement} A ul with error messages.
 */
function arrayToUl(array) {
    const ul = document.createElement("ul");

    const lis = array.map(messageToLi);

    lis.forEach(li => ul.appendChild(li));

    return ul;
}

/**
 * Clears the error messages in the form.
 *
 * @returns void
 */
function clearErrorMessages() {
    errorsFirstNameDiv.innerHTML = "";
    errorsLastNameDiv.innerHTML = "";
    errorsEmailDiv.innerHTML = "";
    errorsPhoneDiv.innerHTML = "";
}

/**
 * Fills in an existing div with a list of error messages.
 *
 * @param {HTMLDivElement} div The div to fill.
 * @param {Array<string>} errorMessages A list of error messages.
 *
 * @returns void
 */
function fillDivWithErrors(div, errorMessages) {
    div.appendChild(arrayToUl(errorMessages));
}

/**
 * Responds to the button click.
 *
 * @param {Event} event the click event.
 *
 * @returns void
 */
function handleFormSubmit(event) {
    let preventSubmit = false;

    clearErrorMessages();

    // If the first name is correctly entered, the 'preventSubmit' is still
    // false and the 'validateName' function returns an empty array.

    // If the first name is entered incorrectly, the 'preventSubmit' becomes
    // true and the 'validateName' function returns an array containing error
    // messages. That array is turned into an unordered list (UL) and the
    // error messages become list items (LIs). They get added to a div.

    const firstNameErrors = validateName(firstNameField.value);
    const thereAreFirstNameErrors = firstNameErrors.length > 0;

    if (thereAreFirstNameErrors) {
        preventSubmit = true;
        fillDivWithErrors(errorsFirstNameDiv, firstNameErrors);
    }

    const lastNameErrors = validateName(lastNameField.value);
    const thereAreLastNameErrors = lastNameErrors.length > 0;

    if (thereAreLastNameErrors) {
        preventSubmit = true;
        fillDivWithErrors(errorsLastNameDiv, lastNameErrors);
    }

    const phoneErrors = validatePhoneNumber(phoneField.value);
    const thereArePhoneErrors = phoneErrors.length > 0;

    if (thereArePhoneErrors) {
        preventSubmit = true;
        fillDivWithErrors(errorsPhoneDiv, phoneErrors);
    }

    const emailErrors = validateEmail(emailField.value);
    const thereAreEmailErrors = emailErrors.length > 0;

    if (thereAreEmailErrors) {
        preventSubmit = true;
        fillDivWithErrors(errorsEmailDiv, emailErrors);
    }

    if (preventSubmit) {
        event.preventDefault();
    }
}

// Tie it all together

form.addEventListener("submit", handleFormSubmit);
