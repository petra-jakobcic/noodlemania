// The utility functions are pure functions which can be used
// on any page in the website.

/**
 * Gets an element by ID.
 *
 * @param {string} id The element ID.
 *
 * @returns {HTMLElement} The element.
 */
function grab(id) {
    return document.getElementById(id);
}

/**
 * Gets the 'add' buttons.
 *
 * @returns {NodeList} The 'add' button elements.
 */
function getTheAddButtons() {
    // Find all elements having a data attribute with the
    // key 'data-product-add'.
    return document.querySelectorAll("[data-product-add]");
}

/**
 * Gets the 'remove' buttons.
 *
 * @returns {NodeList} The 'remove' button elements.
 */
function getTheRemoveButtons() {
    return document.querySelectorAll("[data-product-remove]");
}

/**
 * Gets the quantity stored in a span element.
 *
 * @param {string} spanId The span's ID.
 *
 * @returns {number} The numeric quantity.
 */
function getQuantityFromSpan(spanId) {
    const spanQuantity = grab(spanId);
    return parseInt(spanQuantity.innerHTML);
}

/**
 * Gets the price stored in a span element.
 *
 * @param {string} spanId The span's ID.
 *
 * @returns {number} The numeric quantity.
 */
function getPriceFromSpan(spanId) {
    const spanElement = grab(spanId);

    function numberWithoutDot(string) {
        const stringOfNumericDigits = string.replace(/\./, '');
        return parseInt(stringOfNumericDigits);
    }

    return numberWithoutDot(spanElement.innerHTML);
}

/**
 * Converts a number to a string.
 *
 * @param {number} number The number.
 *
 * @returns {string} The number as a string.
 */
function numberToString(number) {
    return number.toString();
}

/**
 * Adds zeros to the left of any string
 * that is less than 3 characters long,
 * otherwise returns the original string.
 *
 * @param {string} string The string to pad.
 *
 * @returns {string} The padded string, if necessary,
 *                   otherwise the original string.
 */
function padStringLeft(string) {
    return string.padStart(3, "0");
}

/**
 * Adds a dot to the formatted number of pence expressed in pounds.
 *
 * @param {number} pence The amount of pennies.
 *
 * @returns {string} The formatted number of pence as pounds.
 */
function penceToPounds(pence) {
    // Firstly, turn the number into a string.
    const penceAsString = numberToString(pence);

    // Secondly, pad the string with zeros, if needed.
    const paddedPence = padStringLeft(penceAsString);

    // Thirdly, add the dot two spaces before the end of the string.
    return paddedPence.substr(0, paddedPence.length - 2) +
        "." +
        paddedPence.substr(-2);
}

/**
 * Displays the price in British Pounds.
 *
 * @param {number} price The price.
 *
 * @returns {Object} The price in British Pounds.
 */
function displayInPounds(price) {
    const formatter = new Intl.NumberFormat("en-GB", {
        style: "currency",
        currency: "GBP",
        useGrouping: true,
        notation: "standard"
    });

    return formatter.format(price);
    // return new Intl.NumberFormat('en-GB', { style: 'currency', currency: 'GBP' }).format(price);
}

/**
 * Gets all the product elements from the current page.
 *
 * @returns {NodeList} A list of products from the current page.
 */
function getProductsFromPage() {
    // Gets all the elements where the ID starts with 'product'.
    return document.querySelectorAll("[id*='product']");
}

/**
 * Gets the product details based on the given product element.
 *
 * @param {HTMLElement} productElement The product element.
 *
 * @returns {Object} An object containing product details.
 */
function getProductDetails(productElement) {
    let productDetails = {};
    let productId = productElement.id.split("-")[1];

    productDetails.id = productId;
    productDetails.quantity = getQuantityFromSpan("quantity-" + productId);
    productDetails.price = getPriceFromSpan("price-" + productId);

    return productDetails;
}

/**
 * Puts a number as the basket size.
 *
 * @param {number} size The new size to show on the page for the basket.
 *
 * @returns {void}
 */
function updateBasketSize(size) {
    const spanBasket = grab("basket-counter");
    spanBasket.innerHTML = size;
}

/**
 * Puts a numeric quantity inside a span.
 *
 * @param {string} spanId The span's ID.
 * @param {number} quantity The quantity.
 * 
 * @returns {void}
 */
function updateQuantity(spanId, quantity) {
    const span = grab(spanId);
    span.innerHTML = quantity;
}

/**
 * Adds an item to the basket.
 *
 * @param {string} id The product ID.
 * @param {Function} onSuccess A function to call after the item has been
 *                             added to the basket.
 * 
 * @returns {void}
 */
function addItemToBasket(id, onSuccess) {
    // Ajax
    const httpRequest = new XMLHttpRequest();

    // Set up the request
    httpRequest.open("POST", "/basket-items/add");

    // Let the server know that we're sending traditional form data
    // (e.g. firstName=Tom&lastName=Jones&age=32)
    httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    // Prepare for when the response arrives
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                onSuccess();
            } else {
                alert("There was a problem! Please try again or visit the Contact Us page to order by phone.");
            }
        }
    };

    // Send the request to add the food item to the basket
    // Key/value pair
    httpRequest.send(`id=${id}`);
}

/**
 * Removes an item from the basket.
 *
 * @param {string} id The product ID.
 * @param {Function} onSuccess A function to call after the item has been
 *                             removed from the basket.
 *
 * @returns {void}
 */
function removeItemFromBasket(id, onSuccess) {
    // Ajax
    const httpRequest = new XMLHttpRequest();

    // Set up the request
    httpRequest.open("POST", "/basket-items/remove");

    // Let the server know that we're sending traditional form data
    // (e.g. firstName=Tom&lastName=Jones&age=32)
    httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    // Prepare for when the response arrives
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                onSuccess();
            } else {
                alert("There was a problem! Please try again or visit the Contact Us page to order by phone.");
            }
        }
    };

    // Send the request to add the food item to the basket
    // Key/value pair
    httpRequest.send(`id=${id}`);
}
