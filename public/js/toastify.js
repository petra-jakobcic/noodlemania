/**
 * Displays a customised notification when an item is added to
 * the basket.
 *
 * @returns {void}
 */
function toastify() {
  const toast = {
    text: "Item added to basket",
    duration: 2000,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    backgroundColor: "rgba(183,195,147, .8)",
    stopOnFocus: true, // prevents dismissing of toast on hover
    className: "toast"
  };

  Toastify(toast).showToast();
}
