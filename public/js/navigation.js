// Grab the elements we need.

const burgerMenuIcon = document.getElementById("burger-menu");
const navigation = document.getElementById("navigation-container");
const exitButton = document.getElementById("exit-button");

// Write the functions we need.

function handleBurgerMenuIconClick() {
    navigation.classList.add("navigation__container--visible");
}

function handleExitButtonCLick() {
    navigation.classList.remove("navigation__container--visible");
}

// Tie it all together.

burgerMenuIcon.addEventListener("click", handleBurgerMenuIconClick);
exitButton.addEventListener("click", handleExitButtonCLick);
