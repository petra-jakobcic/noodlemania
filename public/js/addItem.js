// This file is included on the 'Food' and the 'Drinks' page.
// It finds all of the 'add' buttons on the page and adds an
// 'event listener' to add that item to the customer's order.

/**
 * Responds to the user's click on an 'add' button.
 *
 * @param {*} event
 */
function handleAddButtonClick(event) {
    const button = event.target;

    const id = button.id;

    addItemToBasket(id, function() {
        // First, get the current basket size.
        let basketSize = getQuantityFromSpan("basket-counter");

        // Second, increase the basket size by 1.
        basketSize++;

        // Third, display the new basket size on the page.
        updateBasketSize(basketSize);
    });

    // Display a customised toast.
    toastify();
}

// Get all the 'add' buttons.
const buttons = getTheAddButtons();

for (let i = 0; i < buttons.length; i++) {
    let currentButton = buttons[i];

    currentButton.addEventListener("click", handleAddButtonClick);
}
