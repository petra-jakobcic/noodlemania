const addButtons = getTheAddButtons();
const removeButtons = getTheRemoveButtons();

/**
 * Responds to the user's click on an 'add' button.
 *
 * @param {*} event
 */
function handleAddButtonClick(event) {
    const button = event.target;
    const id = button.id;

    addItemToBasket(id, function() {
        state.basket.size++;
        state.basket.products[id].quantity++;
        state.basket.totalPrice += state.basket.products[id].price;

        render(state);
    });
}

/**
 * Responds to the user's click on a 'remove' button.
 *
 * @param {*} event 
 */
function handleRemoveButtonClick(event) {
    const button = event.target;
    const id = button.id;

    removeItemFromBasket(id, function() {
        if (state.basket.products[id].quantity > 0) {
            state.basket.size--;
            state.basket.products[id].quantity--;
            state.basket.totalPrice -= state.basket.products[id].price;
        }

        render(state);
    });
}

/***** STATE & RENDER *****/

/*
* The 'state' represents all the pieces of data that can change
* on the current webpage. The 'state' consists of data only.
*
* The 'render' function updates the current webpage based on the
* 'state' data. Keep the 'render' function simple.
*/

// First, create an initial (starting) state.
// This state will be updated after each user interaction.

/**
 * Creates an initial state.
 *
 * @returns {Object} An object structure which holds the
 *                   values of the interactive elements.
 */
function createInitialState() {
    // Get the interactive elements from the page
    const products = {};

    // Get the product elements
    let productElements = getProductsFromPage();

    // Loop through them
    for (let i = 0; i < productElements.length; i++) {
        let productElement = productElements[i];
        // Each time generate the details
        let details = getProductDetails(productElement);
        // Add the details to the 'products' object
        products[details.id] = details;
    }

    // Make a structure to hold their values
    return {
        basket: {
            size: getQuantityFromSpan("basket-counter"),
            products: products,
            totalPrice: getPriceFromSpan("total-price")
        }
    };
}

/**
 * Renders the state data into the appropriate elements on the page.
 *
 * @param {Object} state A structure that contains the changeable
 *                       information found on the page.
 *
 * @returns {void}
 */
function render(state) {
    // Grab all the elements that relate to
    // the data stored in the state
    const basketSizeElement = grab("basket-counter");
    const totalPriceElement = grab("total-price");

    // Update them with the state values
    basketSizeElement.innerHTML = state.basket.size;
    totalPriceElement.innerHTML = penceToPounds(state.basket.totalPrice);

    const products = state.basket.products;

    for (let k in products) {
        let product = products[k];

        if (product.quantity > 0) {
            let spanQuantity = grab("quantity-" + product.id);
            spanQuantity.innerHTML = product.quantity;
        } else {
            let productElement = grab("product-" + product.id);

            if (productElement) {
                let parentElement = productElement.parentNode;

                parentElement.removeChild(productElement);
            }
        }
    }
}

const state = createInitialState();

// Assign an event listener to each 'add' and 'remove' button.

for (let i = 0; i < addButtons.length; i++) {
    let currentButton = addButtons[i];

    currentButton.addEventListener("click", handleAddButtonClick);
}

for (let i = 0; i < removeButtons.length; i++) {
    let currentButton = removeButtons[i];

    currentButton.addEventListener("click", handleRemoveButtonClick);
}
