/* Require your dependencies */

const express = require("express");

const session = require("express-session");
const fileStore = require("session-file-store")(session);

const path = require('path');

const pageRoutes = require("./routes/pageRoutes");
const menuRoutes = require("./routes/menuRoutes");
const formRoutes = require("./routes/formRoutes");
const createBasket = require("./model/basketFunctions");
const shoppingBasketRoutes = require("./routes/shoppingBasketRoutes");

/* Create a server */

const app = express();

/* Use the session middleware */

const optionsForFileStore = {};

app.use(session({
    name: "server-session-cookie-id",
    secret: "shhhhh",
    resave: false,
    saveUninitialized: true,
    store: new fileStore(optionsForFileStore),
    cookie: { maxAge: 60 * 60 * 24 * 1000 }
}));

function shoppingBasketMiddleware(request, response, next) {
    if (!request.session.basket) {
        request.session.basket = [];
    }

    request.shoppingBasket = createBasket(request.session.basket);

    next();
}

app.use(shoppingBasketMiddleware);

/* Make a static folder */

app.use(express.static(path.join(__dirname, "public")));

/* Turn the payload of a POST request from text to an object of key/value pairs */

app.use(express.urlencoded({extended: true}));

/* Prepare the server with its routes */

app.use("/", pageRoutes);
app.use("/menu", menuRoutes);
app.use("/", formRoutes);
app.use("/", shoppingBasketRoutes);

/* Tell Express to use EJS */

app.set("view engine", "ejs");

/* Make the server listen on a port */

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`About to listen on port ${PORT}`);
    console.log(`Now listening on port ${PORT}`);
});
